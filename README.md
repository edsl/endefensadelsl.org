En Defensa del Software Libre
=============================

Revista de Software y Cultura Libres. Hecha
en [Jekyll](http://jekyllrb.com/) y
[Pandoc](http://johnmacfarlane.net/pandoc/) y distribuida por Web,
Torrent y Papel.


## Quiero contribuir

Leer el archivo WORKFLOW, aunque estamos migrando los articulos a un
[repositorio propio](https://0xacab.org/edsl/articulos).


## Pandoc?

Usamos las extensiones de Pandoc para Markdown, por ejemplo
[@cita_bibtex] para referirse al archivo de referencias ref.bib,
~~tachado~~, etc.


## TODO

* Usar javascript para convertir las notas al pie en tooltips

* Encontrar una webfont que se vea mejor

* [Ver issues](https://0xacab.org/edsl/endefensadelsl.org/issues)
